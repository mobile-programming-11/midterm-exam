import 'package:flutter/material.dart';

import 'aboutWebreg_ui.dart';
import 'calendar_ui.dart';
import 'loginScreen.dart';
import 'newStudent.dart';
import 'pressRelease.dart';
import 'requestdoc_ui.dart';
import 'scheduleexamsroomth.dart';
import 'ui_pioneersCard.dart';

class regScreen extends StatelessWidget {
  const regScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: size.height * .45,
            decoration: BoxDecoration(
              color: Color(0xFFF5CEB8),
            ),
          ),
          SafeArea(
              child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 5),
                        child: Expanded(
                          child: Image.network(
                            "https://s.isanook.com/ca/0/rp/r/w728/ya0xa0m1w0/aHR0cHM6Ly9zLmlzYW5vb2suY29tL2NhLzAvdWQvMjc5LzEzOTkzMjMvYnV1LWxvZ28xMV8xLnBuZw==.png",
                            width: 100,
                            height: 100,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(left: 8),
                          child: Text(
                            "ยินดีต้อนรับเข้าสู่ระบบบริการการศึกษา BUU",
                            style: TextStyle(
                              fontSize: 25,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 30),
                  padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(29.5),
                  ),
                ),
                Expanded(
                  child: GridView.count(
                    crossAxisCount: 2,
                    childAspectRatio: .85,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20,
                    children: <Widget>[
                      registerCard(context),
                      calendarCard(context),
                      newstudentCard(context),
                      scheduleexamsroomthCard(context),
                      pioneersCard(context),
                      requestdocCard(context),
                      newsCard(context),
                      aboutWebregCard(context),
                    ],
                  ),
                )
              ],
            ),
          ))
        ],
      ),
    );
  }

  Widget registerCard(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(13),
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 17),
              blurRadius: 17,
              spreadRadius: -20,
            )
          ]),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (BuildContext context) => login_Screen()),
            );
          },
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: <Widget>[
                // Spacer(),
                Image.network(
                  "assets/images/registericonth.png",
                  errorBuilder: (context, url, error) => Icon(Icons.error),
                  width: 150,
                  height: 140,
                  fit: BoxFit.fill,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Widget calendarCard(BuildContext context) {
  return Container(
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(13),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 17),
            blurRadius: 17,
            spreadRadius: -20,
          )
        ]),
    child: Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => calender()),
          );
        },
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              Image.network(
                "assets/images/calendariconth.png",
                errorBuilder: (context, url, error) => Icon(Icons.error),
                width: 150,
                height: 140,
                fit: BoxFit.fill,
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

Widget newstudentCard(BuildContext context) {
  return Container(
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(13),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 20),
            blurRadius: 20,
            spreadRadius: -23,
          )
        ]),
    child: Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => newStudent()),
          );
        },
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              Image.network(
                "assets/images/studenticon.png",
                errorBuilder: (context, url, error) => Icon(Icons.error),
                width: 500,
                height: 150,
                fit: BoxFit.fill,
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

Widget pioneersCard(BuildContext context) {
  return Container(
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(13),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 20),
            blurRadius: 20,
            spreadRadius: -23,
          )
        ]),
    child: Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
                builder: (BuildContext context) => ui_PioneersCard()),
          );
        },
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              Image.network(
                "assets/images/pioneers.png",
                errorBuilder: (context, url, error) => Icon(Icons.error),
                width: 500,
                height: 150,
                fit: BoxFit.fill,
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

Widget requestdocCard(BuildContext context) {
  return Container(
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(13),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 20),
            blurRadius: 20,
            spreadRadius: -23,
          )
        ]),
    child: Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
                builder: (BuildContext context) => requestdoc_ui()),
          );
        },
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              Image.network(
                "assets/images/requestdociconth.png",
                errorBuilder: (context, url, error) => Icon(Icons.error),
                width: 800,
                height: 150,
                fit: BoxFit.fill,
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

Widget newsCard(BuildContext context) {
  return Container(
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(13),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 20),
            blurRadius: 20,
            spreadRadius: -23,
          )
        ]),
    child: Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
                builder: (BuildContext context) => pressRelease()),
          );
        },
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              Image.network(
                "assets/images/newsicon.png",
                errorBuilder: (context, url, error) => Icon(Icons.error),
                width: 500,
                height: 150,
                fit: BoxFit.fill,
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

Widget scheduleexamsroomthCard(BuildContext context) {
  return Container(
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(13),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 20),
            blurRadius: 20,
            spreadRadius: -23,
          )
        ]),
    child: Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
                builder: (BuildContext context) => scheduleexamsroomth()),
          );
        },
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              Image.network(
                "assets/images/scheduleexamsroomth.png",
                errorBuilder: (context, url, error) => Icon(Icons.error),
                width: 500,
                height: 150,
                fit: BoxFit.fill,
              ),
            ],
          ),
        ),
      ),
    ),
  );
}

Widget aboutWebregCard(BuildContext context) {
  return Container(
    decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(13),
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 20),
            blurRadius: 20,
            spreadRadius: -23,
          )
        ]),
    child: Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (BuildContext context) => aboutWebreg()),
          );
        },
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: <Widget>[
              Image.network(
                "assets/images/about_webreg.png",
                errorBuilder: (context, url, error) => Icon(Icons.error),
                width: 500,
                height: 150,
                fit: BoxFit.fill,
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
