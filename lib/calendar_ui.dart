import 'package:flutter/material.dart';

class calender extends StatefulWidget {
  const calender({Key? key}) : super(key: key);

  @override
  State<calender> createState() => _calenderState();
}

class _calenderState extends State<calender> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ปฏิทินการศึกษา'),
      ),
      body: ListView(children: <Widget>[
        Column(children: <Widget>[
          Container(
            width: double.infinity,
            //Height constraint at Container widget level
            height: 180,
            child: Image.network(
              "assets/images/Buu0001.jpeg",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "การยื่นคำร้องขอสำเร็จการศึกษา!!",
                    style: TextStyle(color: Colors.pinkAccent, fontSize: 25),
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 40,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "การยื่นคำร้องขอสำเร็จการศึกษา"
                    "ภาคปลายปีการศึกษา 2565",
                    style: TextStyle(fontSize: 15),
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            //Height constraint at Container widget level
            height: 250,
            child: Image.network("assets/images/grad652.png",
                errorBuilder: (context, url, error) => Icon(Icons.error)),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "กำหนดการชำระค่าธรรมเนียม",
                    style: TextStyle(color: Colors.pinkAccent, fontSize: 30),
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 40,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "การยื่นคำร้องขอสำเร็จการศึกษา"
                    "ปีการศึกษา 2565",
                    style: TextStyle(fontSize: 15),
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: double.infinity,
            //Height constraint at Container widget level
            height: 250,
            child: Image.network("assets/images/pay652.jpg",
                errorBuilder: (context, url, error) => Icon(Icons.error)),
          ),
        ]),
      ]),
    );
    ;
  }
}
