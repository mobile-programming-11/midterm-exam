import 'package:flutter/material.dart';

class scheduleexamsroomth extends StatelessWidget {
  const scheduleexamsroomth({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: buildBodyStdTable(),
    );
  }
}

AppBar buildAppBar(BuildContext context) {
  return AppBar(
    // backgroundColor: Colors.cyan[200],
    backgroundColor: Colors.pink[400],
    leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.home_outlined,
          color: Colors.white,
        )),
    title: Text("ตารางเรียน/สอบ"),
    actions: <Widget>[],
  );
}

Widget buildBodyStdTable() {
  return Container(
    padding: EdgeInsets.all(6),
    child: ListView(
      children: [
        buildSelectYearWidget(),
        buildSelectDurationWidget(),
        Divider(
          color: Colors.pink[400],
        ),
        buildScheduleWidget(),
      ],
    ),
  );
}

Widget buildSelectYearWidget() {
  return Row(
    children: [
      Text('ปีการศึกษา:'),
      IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.arrow_left_rounded,
          )),
      Text(
        '2565',
        style: TextStyle(
            color: Colors.blueAccent[400], fontWeight: FontWeight.bold),
      ),
      IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.arrow_right_rounded,
          )),
    ],
  );
}

Widget buildSelectDurationWidget() {
  return Row(
    children: [
      Icon(Icons.more_vert),
      Text('ภาคเรียน:'),
      IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.looks_one_outlined,
          )),
      Text('/'),
      IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.looks_two_outlined,
            color: Colors.blueAccent,
          )),
      Text('/'),
      IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.sunny,
          )),
      Text('ภาคฤดูร้อน'),
    ],
  );
}

Widget buildScheduleWidget() {
  return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "ตารางเรียนนิสิต",
            textScaleFactor: 1.5,
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.amber.shade100,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.pink.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 20, left: 20, right: 20),
                  child: TextField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: '	โปรดระบุเลขประจำตัวนิสิต',
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20, left: 20, right: 20),
                  child: TextField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: '	ชื่อ',
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20, left: 20, right: 20),
                  child: TextField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'นามสกุล',
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20),
                  child: Container(
                      padding: EdgeInsets.only(top: 5),
                      height: 50,
                      width: 250,
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(20)),
                      child: Center(
                        child: InkWell(
                          onTap: () {},
                          child: Text(
                            'ค้นหา',
                            style: TextStyle(color: Colors.white, fontSize: 30),
                          ),
                        ),
                      )),
                ),
                const SizedBox(
                  width: 300,
                  height: 200,
                  child: Center(
                    child: Text('คำแนะนำ\n'
                        '1. ถ้าต้องการค้นหานิสิตที่มีเลขประจำตัวขึ้นต้นด้วย 41 ให้ป้อน 41*\n'
                        '2. ถ้าต้องการค้นหานิสิตที่มีชื่อขึ้นต้นด้วย สม ให้ป้อน สม*\n'
                        '3. ถ้าต้องการค้นหานิสิตที่มีชื่อลงท้ายด้วย ชาย ให้ป้อน *ชาย\n'
                        '4. ระบุสถานภาพของนิสิต\n'
                        '5. ระบุจำนวนผลลัพธ์ของรายชื่อที่ต้องการ\n'
                        '6. กดปุ่ม เพื่อเริ่มทำการค้นหาตามเงื่อนไข\n'),
                  ),
                ),
              ],
            ),
          ),
        ),
      ]);
}
