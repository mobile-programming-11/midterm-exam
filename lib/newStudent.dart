import 'package:flutter/material.dart';

class newStudent extends StatelessWidget {
  const newStudent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: buildBodyProfileWidget(),
    );
  }
}

AppBar buildAppBar(BuildContext context) {
  return AppBar(
    backgroundColor: Colors.brown[700],
    leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.home_outlined,
          color: Colors.white,
        )),
    title: Text("Student Profile"),
  );
}

Widget buildBodyProfileWidget() {
  return Container(
      padding: EdgeInsets.all(16),
      child: ListView(
        children: <Widget>[
          const SizedBox(
            height: 10,
          ),
          buildCardWidget(),
          const SizedBox(
            height: 10,
          ),
          buildStdCardWidget(),
          const SizedBox(
            height: 10,
          ),
        ],
      ));
}

Widget buildCardWidget() {
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.brown, width: 5),
          ),
        ),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 100,
                  height: 120,
                  child: Image.network(
                      'https://s.isanook.com/ca/0/rp/r/w728/ya0xa0m1w0/aHR0cHM6Ly9zLmlzYW5vb2suY29tL2NhLzAvdWQvMjc5LzEzOTkzMjMvYnV1LWxvZ28xMV8xLnBuZw==.png'),
                ),
                const SizedBox(
                  width: 15,
                ),
                Text('มหาวิทยาลัยบูรพา\n'
                    'Burapha University \n'
                    'คณะวิทยาการสารสนเทศ \n'
                    'Faculty of Informatics \n')
              ],
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}

Widget buildStdCardWidget() {
  return Card(
    child: ClipPath(
      child: Container(
        padding: EdgeInsets.all(16),
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(color: Colors.brown, width: 5),
          ),
        ),
        child: Column(
          children: [
            ListTile(
              leading: Icon(
                Icons.circle_outlined,
                color: Colors.brown[700],
              ),
              title: const Text(
                'คำเตือน',
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
            ),
            Text('ระบบไม่อนุญาตให้ท่านใช้งานในส่วนที่ร้องขอ อาจมีสาเหตุจาก\n'
                '1.	ท่านยังไม่ได้เข้าสู่ระบบ\n'
                '2.	ท่านเข้าสู่ระบบเรียบร้อยแล้ว \nหากท่านไม่ได้ใช้งานระบบนานเกิน 15 นาที ท่านจึงถูกให้ออกจากระบบโดยอัตโนมัติ\n'),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
      clipper: ShapeBorderClipper(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(3))),
    ),
    elevation: 8,
  );
}
