import 'package:flutter/material.dart';

class pressRelease extends StatelessWidget {
  const pressRelease({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: buildBodyStdTable(),
    );
  }
}

AppBar buildAppBar(BuildContext context) {
  return AppBar(
    backgroundColor: Colors.pink[400],
    leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.home_outlined,
          color: Colors.white,
        )),
    title: Text('ประชาสัมพันธ์'),
  );
}

Widget buildBodyStdTable() {
  return Container(
    padding: EdgeInsets.all(6),
    child: ListView(
      children: [
        Divider(
          color: Colors.pink[400],
        ),
        buildScheduleWidget(),
      ],
    ),
  );
}

Widget buildScheduleWidget() {
  return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.amber.shade100,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.pink.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  title: const Text(
                    'ข่าวประชาสัมพันธ์',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  width: 250,
                  height: 150,
                  child: Center(
                      child: Text(
                          'เรื่อง การเทียบเคียงรายวิชา หมวดศึกษาทั่วไป ปรับปรุง พ.ศ. 2554 กับหมวดศึกษาทั่วไป ปรับปรุง พ.ศ. 2559 กลุ่มวิชาสร้างเสริมสุขภาพ เพื่อให้นิสิตตกค้างในรายวิชา 850101-850125')),
                ),
              ],
            ),
          ),
        ),
      ]);
}
