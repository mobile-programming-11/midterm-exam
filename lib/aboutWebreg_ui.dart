import 'package:flutter/material.dart';

class aboutWebreg extends StatelessWidget {
  const aboutWebreg({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: buildBodyStdTable(),
    );
  }
}

AppBar buildAppBar(BuildContext context) {
  return AppBar(
    backgroundColor: Colors.pink[400],
    leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.home_outlined,
          color: Colors.white,
        )),
    title: Text("ติดต่อเรา"),
    actions: <Widget>[],
  );
}

Widget buildBodyStdTable() {
  return Container(
    padding: EdgeInsets.all(6),
    child: ListView(
      children: [
        Divider(
          color: Colors.pink[400],
        ),
        buildScheduleWidget(),
      ],
    ),
  );
}

Widget buildScheduleWidget() {
  return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.amber.shade100,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.pink.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  title: const Text(
                    'เวลาทำการ',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  width: 300,
                  height: 200,
                  child: Center(
                      child: Text('เปิดภาคเรียน\n'
                          '◉ 08:30 - 19:30 น. (จันทร์ พุธ ศุกร์)\n'
                          '◉ 08:30 - 16:30 น. (อังคาร พฤหัสบดี เสาร์ อาทิตย์)\n'
                          'หมายเหตุ: ปิดวันหยุดราชการ\n'
                          'ปิดภาคเรียน\n'
                          '◉ 08:30 - 16:30 น. (จันทร์ - ศุกร์)\n'
                          '◉ ปิดเสาร์ - อาทิตย์\n')),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.green.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  title: const Text(
                    'ที่อยู่',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  width: 300,
                  height: 150,
                  child: Center(
                      child: Text('กองทะเบียนและประมวลผลการศึกษา\n'
                          'มหาวิทยาลัยบูรพา\n'
                          '169 ถ.ลงหาดบางแสน\n'
                          'ต.แสนสุข อ.เมือง จ.ชลบุรี 20131\n')),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.blue.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  title: const Text(
                    'เบอร์โทรศัพท์',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  width: 200,
                  height: 200,
                  child: Center(
                      child: Text('โทรศัพท์ (เคาน์เตอร์)\n	0 3810 2725\n'
                          '\nโทรสาร\n	0 3839 0441, 0 3810 2721\n'
                          '\nโทรศัพท์ (เจ้าหน้าที่)	\nฝ่ายรับเข้าศึกษาระดับปริญญาตรี\n'
                          'โทร. 038-102721\n หรือ 038-102643')),
                ),
              ],
            ),
          ),
        ),
      ]);
}
