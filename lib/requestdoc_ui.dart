import 'package:flutter/material.dart';

class requestdoc_ui extends StatelessWidget {
  const requestdoc_ui({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: buildBodyStdTable(),
    );
  }
}

AppBar buildAppBar(BuildContext context) {
  return AppBar(
    backgroundColor: Colors.pink[400],
    leading: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: Icon(
          Icons.home_outlined,
          color: Colors.white,
        )),
    title: Text("แบบคำร้อง (Request form)"),
    actions: <Widget>[],
  );
}

Widget buildBodyStdTable() {
  return Container(
    padding: EdgeInsets.all(6),
    child: ListView(
      children: [
        Divider(
          color: Colors.pink[400],
        ),
        buildScheduleWidget(),
      ],
    ),
  );
}

Widget buildScheduleWidget() {
  return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.amber.shade100,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.pink.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  title: const Text(
                    'RE01   คำร้องทั่วไป',
                    style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  width: 100,
                  height: 100,
                  child: Center(
                      child: Text('https://reg.buu.ac.th/regisform/RE01.pdf')),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.green.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  title: const Text(
                    'RE02   คำร้องขอเงินค่าหน่วยกิตคืน',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  width: 100,
                  height: 100,
                  child: Center(
                      child: Text('https://reg.buu.ac.th/regisform/RE02.pdf')),
                ),
              ],
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Card(
            elevation: 10,
            color: Colors.blue.shade50,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50),
            ),
            child: Column(
              children: [
                ListTile(
                  title: const Text(
                    'RE03   คำร้องขอใบแสดงผลการเรียน/ใบรับรอง/บัตรนิสิต',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(
                  width: 100,
                  height: 100,
                  child: Center(
                      child: Text('https://reg.buu.ac.th/regisform/RE02.pdf')),
                ),
              ],
            ),
          ),
        ),
      ]);
}
